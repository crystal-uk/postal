<?php
namespace crystal\postal\services;

use crystal\postal\Postal;
use crystal\postal\models\Envelope;


use Craft;
use craft\base\Component;
use craft\mail\Message;


class Mail extends Component {
	
	public function getConfig($form) {
		$settings = Postal::$app->getSettings();

		if ($specific = is_array($form) ? $form : ($settings->attributes['forms'][$form] ?? false)) {
			$settings->attributes = $specific;
		}

		return $settings;
	}

	public function build($letter, $config) {
		$envelope = new Envelope(compact('letter', 'config'));
		$message = new Message();
    
		$details = ['to', 'from', 'cc', 'bcc', 'replyTo', 'subject', 'textBody', 'htmlBody'];
		
    // run message->setDetail($envelope->detail) for each
    array_walk($details, function($detail) use ($envelope, $message) {
			if ($envelope[$detail]) {
				$func = 'set'.ucfirst($detail);
        $message->$func($envelope[$detail]);
      }
    });
		
    return $message;
	}
	
	public function send($message) {
		if (Craft::$app->request->queryParams['debug'] ?? false) {
      echo '<pre>';
      print_r($message->toString());
      die();
    }
    
    return Craft::$app->mailer->send($message);
	}

	public function error($message, $errors = false) {
		Craft::$app->getResponse()->setStatusCode('400');

		$output = [
			'type' => 'error',
			'message' => $message,
		];

		if ($errors) {
			$output['errors'] = $errors;
		}
 
		return $output;
	}
}
