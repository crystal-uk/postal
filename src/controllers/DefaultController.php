<?php
namespace crystal\postal\controllers;

use crystal\postal\Postal;

use yii\base\{DynamicModel, InvalidArgumentException};
use yii\filters\{ContentNegotiator, Cors};

use Craft;
use craft\web\{Controller, Response};
use craft\helpers\Json;


class DefaultController extends Controller {
  protected $allowAnonymous = true;
  public $enableCsrfValidation = false;

  public function actionIndex() {
    $headers = Craft::$app->getResponse()->getHeaders();
    $headers->set('Access-Control-Allow-Origin', '*');
    $headers->set('Access-Control-Allow-Headers', 'content-type');

    $method = Craft::$app->request->method;

    if ($method === 'OPTIONS') {
      $headers->set('Allow', 'POST, OPTIONS');
      return '';
    } else if ($method !== 'POST') {
      return Postal::$app->mail->error('Postal only accepts post posted by POST');
    }

    try {
      $post = Json::decode(Craft::$app->request->rawBody, true);
    } catch (InvalidArgumentException $e) {
      return Postal::$app->mail->error('Invalid JSON');
    }

    $config = Postal::$app->mail->getConfig($post['form'] ?? '');

    if (!$config->validate()) {
      return Postal::$app->mail->error('Config file is incorrect', $config->errors);
    }

    // TODO: merge in default rules
    $letter = DynamicModel::validateData($config['fields'], $config['rules']);
    $letter->attributes = $post;

    if (!$letter->validate()) {
      return Postal::$app->mail->error('Posted data is incorrect', $letter->errors);
    }

    $message = Postal::$app->mail->build($letter, $config);

    if ($config->response) {
      $conf = Postal::$app->mail->getConfig($config->response);

      if (!$conf->validate()) {
        return Postal::$app->mail->error('Config file is incorrect', $conf->errors);
      }

      $response = Postal::$app->mail->build($letter, $conf);

      Postal::$app->mail->send($response);
    }

    return Postal::$app->mail->send($message);
  }

  public function behaviors() {
    return [
      'contentNegotiator' => [
        'class' => ContentNegotiator::class,
        'formats' => [
          'application/json' => Response::FORMAT_JSON,
        ],
      ],
    ];
  }
}
