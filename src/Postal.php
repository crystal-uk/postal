<?php
namespace crystal\postal;

use crystal\postal\models\Settings;
use crystal\postal\services\Mail;

use Craft;
use craft\base\Plugin;

use yii\base\Event;

class Postal extends Plugin {
	public static $app;

	public $schemaVersion = '2';

	public function init() {
		parent::init();
		self::$app = $this;

		$this->setComponents([
			'mail' => Mail::class,
		]);
	}

	protected function createSettingsModel() {
		return new Settings();
	}
}
