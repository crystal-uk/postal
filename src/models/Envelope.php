<?php
namespace crystal\postal\models;

use crystal\postal\Postal;

use Craft;
use craft\base\Model;
use craft\web\View;


class Envelope extends Model {

  public $config;
  public $defaults = [
    'subject' => 'Enquiry from {letter.name}',
  ];
  public $letter;

  public function init() {
    parent::init();

    $craft = Craft::$app->systemSettings->getSettings('email');
    $this->defaults['from'] = [$craft['fromEmail'] => $craft['fromName']];
    
    unset($this->config['forms']);
  }

  public function getTextBody() {
    return $this->render();
  }
  
  public function getHtmlBody() {
    return $this->render(true);
  }

  public function getFrom() {
    return $this->run('from') ?? $this->defaults['from'];
  }

  public function getTo() {
    return $this->run('to');
  }

  public function getSubject() {
    return Craft::$app->view->renderObjectTemplate(
      $this->run('subject') ?? $this->defaults['subject'], 
      $this
    );
  }

  public function getReplyTo() {
    return $this->run('replyTo');
  }

  public function getCc() {
    return $this->run('cc');
  }

  public function getBcc() {
    $bcc = $this->run('bcc');

    if ($bcc) {
      $bcc = array_merge(is_array($bcc) ? $bcc : [$bcc]);
    }
    if ($this->run('debug') ?? false) {
      $bcc[] = 'debug@wearecrystal.uk'; 
    }

    return $bcc;
  }

  public function getLabeled() {
    $letter = $this->letter->attributes;

    return array_reduce(array_keys($letter), function ($carry, $key) use ($letter) {

      $pretty = ucfirst(strtolower(join(' ', preg_split('/(?<=[a-z])(?=[A-Z])/x', $key))));

      $carry[$pretty] = $letter[$key];
      return $carry;
    }, []);
  }

  private function render($html = false) {
    $path = $this->run('template');

    if (!$path) {
      Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_CP);
      $path = 'postal/email';
    }

    return Craft::$app->view->renderTemplate($path, 
      ['letter' => $this->labeled, 'html' => $html]
    );
  }

  private function run($attribute) {
    $detail = $this->config[$attribute];

    if (is_callable($detail)) {
      return $detail($this->letter);
    } else {
      return $detail;
    }
  }

}
