<?php
namespace crystal\postal\models;

use crystal\postal\Postal;

use Craft;
use craft\base\Model;


class Settings extends Model {

	public $forms;

	public $template;
	public $receipt;
	public $debug;

	public $to;
	public $from;
	public $cc;
	public $bcc;
	public $replyTo;
	public $subject;
	public $response;
	
	public $fields;
	public $rules;

	public function rules() {
		return [
			[['to', 'fields'], 'required'],
			['fields', 'each', 'rule' => ['string']],
			[['template', 'receipt', 'debug', 'from', 'cc', 'bcc', 'replyTo', 'subject', 'rules', 'response'], 'safe'],
		];
	}
}
